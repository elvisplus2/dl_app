require 'rubygems'
require 'bundler/setup'
require 'sinatra/json'

Bundler.require

Dir['./models/*.rb'].each do |file_name|
  require file_name
end


class App < Sinatra::Application
  Sinatra::Delegator.delegate

  configure do
    register Sinatra::AssetPipeline
    set :database, { adapter: 'sqlite3', database: 'db/db.sqlite3' }
  end


  get '/' do

    haml :index
  end

  post '/calculate.json' do
    request.body.rewind
    payload = JSON.parse(request.body.read)['payload']

    calculation = Calculation.new({ start_point: payload['startPoint'],
                                    end_point:   payload['endPoint'],
                                    volume:      payload['volume'],
                                    distance:    payload['distance'],
                                    weight:      payload['weight'],
                                    dimensions: [ payload['width'], payload['length'], payload['height'] ]
                                  })

    if calculation.save
      json calculation
    else
      status 422
      json calculation.errors
    end
  end

  get '/top' do
    @top = Calculation.select('count(*) as count_all, start_point').group(:start_point).order('count_all desc').limit(3)

    haml :top
  end

end
