app = angular.module('calc_app', [])

app.controller 'appController', ['$scope', '$http',
  ($scope, $http) ->
    $scope.map        = {}
    $scope.startPoint = {}
    $scope.endPoint   = {}
    $scope.history = []

    $scope.payload = {
      width: null
      height: null
      length: null
      weight: null
      distance: null
      startPoint: null
      endPoint: null
      calcVolume: ->
        this.volume = Math.round this.width * this.length * this.height
      volume: null
    }

    ymaps.ready ->
      $scope.map = new ymaps.Map('map', {
        center: [60.906882, 30.067233],
        zoom: 9,
        type: 'yandex#map',
        controls: []
      })

      searchStartPoint = new ymaps.control.SearchControl({
        options: {
          useMapBounds: true,
          noPlacemark: true,
          noPopup: true,
          placeholderContent: 'Адрес начальной точки',
          size: 'large'
        }
      })

      searchEndPoint = new ymaps.control.SearchControl({
        options: {
          useMapBounds: true,
          noCentering: true,
          noPopup: true,
          noPlacemark: true,
          placeholderContent: 'Адрес конечной точки',
          size: 'large',
          float: 'none',
          position: {left: 10, top: 44}
        }
      })


      $scope.map.controls.add searchStartPoint
      $scope.map.controls.add searchEndPoint

      searchStartPoint.events.add('resultselect',
        (e) ->
          $scope.setStartPoint searchStartPoint.getResultsArray()[e.get('index')]
      ).add('load',
        (event) ->
          if !event.get('skip') && searchStartPoint.getResultsCount()
            searchStartPoint.showResult(0);
      )

      searchEndPoint.events.add('resultselect',
        (e) ->
          $scope.setEndPoint searchEndPoint.getResultsArray()[e.get('index')]
      ).add('load',
        (event) ->
          if !event.get('skip') && searchEndPoint.getResultsCount()
            searchEndPoint.showResult(0);
      )


    $scope.setStartPoint = (point) ->
      $scope.startPoint         = point
      $scope.payload.startPoint = point.properties.get('name')

      if $scope._startMarker
        $scope.map.geoObjects.remove $scope._startMarker

      $scope._startMarker = new ymaps.Placemark(point.geometry.getCoordinates(), {iconContent: 'A'})

      $scope.map.geoObjects.add $scope._startMarker

      $scope.setRoute()
      $scope.$digest()

    $scope.setEndPoint = (point) ->
      if $scope._endMarker
        $scope.map.geoObjects.remove $scope._endMarker

      $scope.endPoint         = point
      $scope.payload.endPoint = point.properties.get('name')

      $scope._endMarker = new ymaps.Placemark(point.geometry.getCoordinates(), {iconContent: 'B'})

      $scope.map.geoObjects.add $scope._endMarker

      $scope.setRoute()
      $scope.$digest()


    $scope.setRoute = ->
      if $scope._route
        $scope.map.geoObjects.remove $scope._route

      if !$.isEmptyObject($scope.startPoint) && !$.isEmptyObject($scope.endPoint)
        ymaps.route(
          [$scope.startPoint.geometry.getCoordinates(), $scope.endPoint.geometry.getCoordinates()]
        ).then (router)->
          $scope._route = router.getPaths()

          $scope.map.geoObjects.add $scope._route
          $scope.map.setBounds($scope.map.geoObjects.getBounds())

          $scope.payload.distance = Math.round router.getLength() / 1000
          $scope.$digest()

    $scope.calculate = (form)->
      if !form.$valid || !$scope.payload.endPoint || !$scope.payload.startPoint
        alert 'Форма заполнена не правильно.'
      else
        $http.post('/calculate.json', payload: $scope.payload)
        .success(
          (data)->
            $scope.history.push data
        )
        .error(
          (data) ->
            console.log(data)
        )
]
