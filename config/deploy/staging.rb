set :stage, :staging

# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
role :app, %w{elvisplus2@coolelvis.me}
role :web, %w{elvisplus2@coolelvis.me}
role :db,  %w{elvisplus2@coolelvis.me}

set :branch, 'master'
# set :rails_env, 'development'

#set :migration_role, 'migrator'            # Defaults to 'db'
# set :assets_roles, [:web, :app]            # Defaults to [:web]
# set :assets_prefix, 'prepackaged-assets'   # Defaults

set :application, 'dl_app'
set :repo_url, 'git@github.com:CoolElvis/dl_app.git'
set :deploy_to, '/home/elvisplus2/dl_app'
