class CreateCalculation < ActiveRecord::Migration
  def change
    create_table :calculations do |t|
      t.string :start_point, null: false
      t.string :end_point, null: false
      t.integer :weight, null: false
      t.integer :volume, null: false
      t.integer :distance, null: false
      t.integer :cost, null: false

      t.string :dimensions , default: '[]'
    end
  end
end
