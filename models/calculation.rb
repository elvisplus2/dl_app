require 'sinatra/activerecord'

class Calculation < ActiveRecord::Base
  validates_presence_of :start_point
  validates_presence_of :end_point

  validates :distance, format: { with: /[1-9]\d*/, message: 'Укажите положительное целое число.' }
  validates :volume, format:   { with: /[1-9]\d*/, message: 'Укажите положительное целое число.' }
  validates :weight, format:   { with: /[1-9]\d*/, message: 'Укажите положительное целое число.' }

  before_save :calculate

  def calculate
    self.cost = volume * distance * weight
  end

end
